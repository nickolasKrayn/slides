import { createGlobalStyle } from 'styled-components/macro';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export const GlobalStyle = createGlobalStyle`
#root {
    height: 100%;
    position: relative;
  }
  #root:before{
    content: "";
    position: absolute;
    z-index: 1;
    width: 100%;
    top: 0;
    pointer-events: none;
    background-image: linear-gradient(to top, transparent 0%, rgba(0, 0, 0, 0.15) 130%);
    height: 20vh;
  }
  html {
    height: 100%;
    overflow: auto;
    margin: 0;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif;
  -webkit-font-smoothing: subpixel-antialiased;
  -moz-osx-font-smoothing: grayscale;
  }
  

  body {
    position: relative;
    margin: 0;
    min-width: 320px;
    height: 100%;
    overflow: auto;
    width: 100%;
    font-size: 14px;
    touch-action: none;
  }

  body,
  html {
    width: 100%;
    height: 100%;
    margin: 0;
    overflow: auto;
  }
  
  .slick-slider {
    overflow: hidden;
  }
  .slick-slider, .slick-list, .slick-track, .slick-slide, .slick-slide > div {
    height: 100% !important;
    width: 100% !important;
  }
  .slick-list {
    padding: 0px !important;
  }
  .slick-arrow {
    z-index: 500;
    top: 30px;
  }
  .slick-prev {
    left: 25px;
  }
  .slick-next {
    right: 25px;
  }
  .slide-container {
    height: 100%;
    width: 100%;
  }
  .slide-container > div {
    height: 100%;
    width: 100%;
    display: none;
  }
`;
