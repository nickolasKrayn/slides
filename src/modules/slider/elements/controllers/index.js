import icon_right from './img/arrow-right-line.svg'
import icon_left from './img/arrow-left-line.svg'
import React from 'react'

export const PrevNextControllers = ({ prevClick, nextClick, disablePrev, disableNext }) => {
    return (<div style={{ position: 'absolute', width: '100%', left: '0', top: '0', zIndex: '9' }}><div style={{
        width: '100%',
        color: '#fff'
    }}>
    <div onClick={prevClick} style={{
        padding: '15px',
        cursor: 'pointer',
        position: 'absolute',
        left: '5px',
        top: '20px',
        ...(disablePrev && { display: 'none' }) }}>
        <img src={icon_left} />
    </div>
    <div  onClick={nextClick} style={{
        padding: '15px',
        cursor: 'pointer',
        position: 'absolute',
        right: '5px',
        top: '20px',
        ...(disableNext && { display: 'none' }) }}>
        <img src={icon_right} />
    </div>
    </div></div>)
}