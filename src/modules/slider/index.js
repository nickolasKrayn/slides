import React, { useState } from 'react'
import Slick from 'react-slick'
import { Slide } from './components/Slide/index'
import { FakeSlider as _FakeSlider } from './components/FakeSlider'

import { StoryEditor } from 'modules/storyEditor';
import { StoryControls } from 'modules/storyControls'
import { useEffect } from 'react';

import { createUUID } from 'utils/createUUID'

const settings = {
    centerMode: false,
    dots: false,
    infinite: false,
    speed: 300,
    touchThreshold: 15,
    adaptiveHeight: false,
    variableWidth: false,
    swipe: false,
    centerMode: true,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1
  };

export const Slider = ({ slides }) => {
    return <Slick {...settings}>
            {slides.map((item, index) => <Slide key={`slide-${index}`}>
                {item.content}
</Slide>)}
        </Slick>
}

export const FakeSlider = ({ slides, activeSlide, setActiveSlide, addSlide, removeSlide }) => { // TODO: Временно, сделать нормально
  const [elements, setElements] = useState(slides.map(item => ({
    id: item.id,
    elements: {
      elements: {},
      elementsRef: []
    }
  })))

  useEffect(() => {
    console.log(elements)
  }, [elements])

  const changeElements = (index, items) => {
    const newElements = [...elements]
    newElements[index].elements = items
    setElements(newElements)
  }

  return <_FakeSlider
      prevClick={() => {
        setActiveSlide(activeSlide - 1)
      }}
      nextClick={() => {
        setActiveSlide(activeSlide + 1)
      }}
      addSlide={() => {
        const slideId = createUUID()
        setElements([
          ...elements,
          {
            id: slideId,
            elements: {
              elements: {},
              elementsRef: []
            }
          }     
        ])
        addSlide(slideId)
        setActiveSlide(slides.length)
      }}
      setActiveSlide={setActiveSlide}
      points={slides.length}
      activeSlide={activeSlide}
      onRemove={() => {
        if (slides.length === 1) {
          let newElements = [...elements]
          newElements[0].elements.elements = {}
          newElements[0].elements.elementsRef = []
          setElements(newElements)
          return
        }

        const slideId = slides[activeSlide].id

        setElements(elements.filter(element => slideId !== element.id))
        removeSlide(slideId)
      }}
    >
    {slides.map((item, index) => <div style={activeSlide === index ? { display: 'block' } : {}} key={`slide-${index}`}>
                {item.contentType === 'storyEditor' && elements[index] && <StoryEditor
                  items={elements[index].elements}
                  onChangeElements={(elements) => { changeElements(index, elements) }}
                  onRemove={() => {
                    /*if (slides.length === 1) {
                      return
                    }
                    setElements(elements.filter(element => item.id !== element.id))
                    removeSlide(item.id)*/
                  }}
                />}
                {/*item.contentType === 'storyControls' && <StoryControls onAdd={addSlide} />*/}
    </div>)}
  </_FakeSlider>
}
