import { Slide as _Slide } from './style'
import React from 'react'

export const Slide = (props) => <_Slide {...props} />