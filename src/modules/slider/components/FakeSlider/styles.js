import styled from 'styled-components/macro';

export const Slide = styled.div`
    transition: all .2s;
    margin: 0 3px;
    z-index: 10;
    flex: 1 0 auto;
    padding: 12px 0 15px!important;
    cursor: pointer;
    & > div  {
      background-color: hsla(0,0%,100%,.4);
      height: 4px!important;
      border-radius: 10px;
      transition: all .2s;
    }
    &.active-slide > div {
      background-color: #fff!important;
    }
  }
`;
export const SlideWrapper = styled.div`
  position: absolute;
  top: 0;
  width: calc(100% - 40px);
  left: 0;
  display: flex;
  justify-content: center;
  fontSize: 50px;
  zIndex: 5;
  padding: 0 20px;
`;