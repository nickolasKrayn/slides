import React from 'react'
import { PrevNextControllers } from './../../elements/controllers'
import { StoryControls } from './../../../storyControls'
import { Slide, SlideWrapper } from './styles';

const Points = ({ activeSlide, setActiveSlide, count }) => {
    const points = [];
    for(let i = 0; i < count; i++) {
        points.push(<Slide className={(activeSlide === i ?  'active-slide' :  '' )}
            onClick={() => {
                if (i !== activeSlide) {
                    setActiveSlide(i)
                }
            }}
            key={`point-${i}`}><div></div></Slide>)
    }
    return points
}

export const FakeSlider = ({ activeSlide, prevClick, nextClick, setActiveSlide, points, addSlide, onRemove, children }) => {
    return <>
        <SlideWrapper>
            <Points setActiveSlide={setActiveSlide} activeSlide={activeSlide} count={points} />
        </SlideWrapper>
        <PrevNextControllers
            disablePrev={activeSlide === 0}
            disableNext={activeSlide === (points - 1)}
            prevClick={prevClick}
            nextClick={nextClick}
        />
        <StoryControls onAdd={addSlide} onRemove={onRemove} />
        <div className='slide-container'>{children}</div>
    </>
}