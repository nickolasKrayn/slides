import React from 'react'
import styled from 'styled-components/macro'

const TextInput = styled.span`
  display: block;
  text-align: left;
  word-wrap: break-word;
  color: white;
  text-shadow: rgba(150, 150, 150, 0.3) 0px 1px 2px;
  font-size: 20px;
  font-weight: 400;
  line-height: 32px;
  outline: 0;
  padding: 5px 0 5px;
  margin: 0 auto 20px;
  width: 100%;
  -webkit-user-select: text;
  user-select: text;
  border-bottom: 1px solid #3da773;
  &:focus {
    border-color: #338b60;
  }
`;

const SimpleText = styled.span`
  display: block;
  text-align: left;
  word-wrap: break-word;
  color: white;
  text-shadow: rgba(150, 150, 150, 0.3) 0px 1px 2px;
  font-size: 20px;
  font-weight: 400;
  line-height: 28px;
  outline: 0;
  padding: 10px 0 10px;
  margin: 0 auto 20px;
  margin: 0 auto;
  width: 100%;
  border-bottom: 1px solid #3da773;
  &:focus {
    border-color: #338b60;
  }
`;

export const Text = React.forwardRef((props, ref) => {
  return <TextInput {...props} ref={ref} />;
})

/*export default (props) => {
    return <Text {...props} /> 
}*/