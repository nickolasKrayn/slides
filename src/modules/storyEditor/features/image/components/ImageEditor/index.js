import React, { useState } from 'react'
import { Overlay, TextInput, Label, Container, ButtonEditorWrapper } from './styles';
import { Button } from 'modules/storyEditor/components/Actions/styles';
import { ImageUploader } from 'modules/storyEditor/components/ImageUploader'
import Warning from './../../../../../../elements/Warning'
import icon_del from './../../../_img/delete-bin-line.svg'
import icon_check from './../../../_img/check-line.svg'

const _ImageEditor = ({ id, initialContent, onSubmit, onRemove }) => {
    const [image, setImage] = useState()
    const imageUploaderRef = React.createRef()
    const handleRemove = () => {
        onRemove(id)
    }

    const handleSubmit = () => {
        onSubmit(image)
    }

    return <Overlay>
        <ButtonEditorWrapper>
            {id && <Warning onAction={handleRemove}>
            <Button style={{position: 'absolute', left: '15px'}}><img src={icon_del} /></Button>
        </Warning>}
        <Button onClick={handleSubmit} style={{position: 'absolute', right: '15px'}}><img src={icon_check} /></Button>
      </ButtonEditorWrapper>
      <Container>
          <img onClick={() => {
              imageUploaderRef.current.open()
          }} src={image || initialContent} style={{ width: '80%', margin: '0 auto', marginTop: '40px', display: 'block' }} />
      </Container>
      <ImageUploader ref={imageUploaderRef} onImageSelect={(image) => {
          setImage(URL.createObjectURL(image))
      }} />
    </Overlay>
}

export const ImageEditor = _ImageEditor