import React, { useState, useEffect } from 'react'
import { Overlay, TextInput, Label, Container, ButtonEditorWrapper } from './styles';
import { Button } from 'modules/storyEditor/components/Actions/styles';
import { VideoUploader } from 'modules/storyEditor/components/VideoUploader'
import Warning from './../../../../../../elements/Warning'
import icon_del from './../../../_img/delete-bin-line.svg'
import icon_check from './../../../_img/check-line.svg'

const _VideoEditor = ({ id, initialContent, onSubmit, onRemove }) => {
    const [video, setVideo] = useState()
    const videoRef = React.createRef()
    const videoUploaderRef = React.createRef()
    const handleRemove = () => {
        onRemove(id)
    }

    const handleSubmit = () => {
        onSubmit(video)
    }

    useEffect(() => {
        videoRef.current.load()
        videoRef.current.play()
    }, [video])

    return <Overlay>
        <ButtonEditorWrapper>
            {id && <Warning onAction={handleRemove}>
            <Button style={{position: 'absolute', left: '15px'}}><img src={icon_del} /></Button>
        </Warning>}
            <Button onClick={handleSubmit} style={{position: 'absolute', right: '15px'}}><img src={icon_check} /></Button>
      </ButtonEditorWrapper>
      <Container>
          <div
            style={{  height: '100%' }}
            onClick={() => {
                videoUploaderRef.current.open()
            }}
        >
            <video ref={videoRef} style={{ height: '80%', margin: '25px auto', display: 'block' }} preload={"true"} autoplay playsinline muted loop>
                <source src={video || initialContent}  type={'video/mp4;codecs="avc1.42E01E, mp4a.40.2"'}/>
            </video>
          </div>
      </Container>
      <VideoUploader ref={videoUploaderRef} onVideoSelect={(video) => {
          setVideo(URL.createObjectURL(video))
      }} />
      {/*<ImageUploader ref={imageUploaderRef} onImageSelect={(image) => {
          setImage(URL.createObjectURL(image))
      }} />*/}
    </Overlay>
}

export const VideoEditor = _VideoEditor