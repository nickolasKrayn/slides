import styled from 'styled-components/macro';

import { Text } from 'modules/storyEditor/components/elements/Text';
import { Wrapper } from 'modules/storyEditor/components/Actions/styles';

export const Overlay = styled.div`
  display: flex;
  position: absolute;
  top: 0;
  left: 0;
  flex-direction: column;
  align-items: center;
  overflow: hidden;
  height: 100%;
  width: 100%;
  background: #46bc82;
  z-index: 16;
`;

export const Container = styled.div`
  display: block;
  width: 100%;
  height: 100%;
  padding: 0px 15px 0px 15px;
  margin-top: 50px;
`

export const Label = styled.div`
  display: block;
  width: 100%;
  font-size: 20px;
  color: #fff;
  opacity: .85;
`

export const TextInput = styled(Text)`
  margin: 10px auto;
  -webkit-user-select: text;
  user-select: text;
`;

export const ButtonEditorWrapper = styled(Wrapper)`
  margin-top: 5px;
  width: 100%;
  right: 0;
`;

export const ColorContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-around;
`

export const Color = styled.div`
  width: 20%;
  display: flex;
`
