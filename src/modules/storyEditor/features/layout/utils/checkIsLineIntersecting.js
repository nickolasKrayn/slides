const EPSILON = 1e-6;
const INSIDE = true;
const OUTSIDE = false;

const clipT = (num, denom, c) => {
  const [tE, tL] = c;
  if (Math.abs(denom) < EPSILON) return num < 0;
  const t = num / denom;

  if (denom > 0) {
    if (t > tL) return 0;
  } else {
    if (t < tE) return 0;
  }
  return 1;
};

// box [xmin, ymin, xmax, ymax]
export const checkIsLineIntersecting = (a, b, box) => {
  const [x1, y1] = a;
  const [x2, y2] = b;
  const dx = x2 - x1;
  const dy = y2 - y1;

  if (
    Math.abs(dx) < EPSILON &&
    Math.abs(dy) < EPSILON &&
    x1 >= box[0] &&
    x1 <= box[2] &&
    y1 >= box[1] &&
    y1 <= box[3]
  ) {
    return INSIDE;
  }

  const c = [0, 1];
  if (
    clipT(box[0] - x1, dx, c) &&
    clipT(x1 - box[2], -dx, c) &&
    clipT(box[1] - y1, dy, c) &&
    clipT(y1 - box[3], -dy, c)
  ) {
    return INSIDE;
  }
  return OUTSIDE;
};
