import styled from 'styled-components/macro';

export const DraggableArea = styled.div`
  z-index: 2;
  display: block;
  position: absolute;
  max-width: 100%;
  touch-action: none;
  -webkit-tap-highlight-color: transparent;
  user-select: none;
`;

export const Background = styled.img`
  display: block;
  outline: 0;
  border: 0;
  margin: 0;
  padding: 0;
  object-fit: cover;
  width: 100%;
  height: 100%;
`;
