import styled from 'styled-components/macro';

export const DraggableArea = styled.div`
  z-index: 2;
  display: block;
  position: absolute;
  background: #338b60;
  width: 100%;
  touch-action: none;
  -webkit-tap-highlight-color: transparent;
  user-select: none;
`;

export const Background = styled.video`
  display: block;
  outline: 0;
  border: 0;
  margin: 0;
  padding: 0;
  width: 100vw;
  height: 100vh;
  object-fit: cover;
`;
