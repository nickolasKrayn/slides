import React, { useEffect, useRef, memo, useState } from 'react';

import { draggablePropTypes } from '../../utils/draggablePropTypes';

import { Background, DraggableArea } from './styles';

const _DraggableVideo = ({
  id,
  x,
  y,
  content,
  transform,
  parentWidth,
  parentHeight,
}) => {
  const ref = useRef();
  const videoRef = useRef();
  const isPositionSet = useRef(false);
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    if (videoRef.current) {
      if (videoRef.current.complete) {
        setIsLoaded(true);
      } else {
        videoRef.current.onload = () => {
          setIsLoaded(true);
        };
      }
    }
  }, []);

  useEffect(() => {
    videoRef.current.play()
  }, [content])

  useEffect(() => {
    const element = ref.current;
    if (
      ref.current &&
      !isPositionSet.current &&
      parentHeight > 0 &&
      parentWidth > 0 &&
      isLoaded
    ) {
      if (x || y) {
        element.style.top = y;
        element.style.left = x;
      } else {
        element.style.left = `${
          50 - (element.clientWidth / parentWidth / 2) * 100
        }%`;
        element.style.top = `${
          50 - (element.clientHeight / parentHeight / 2) * 100
        }%`;
      }
      element.style.transform = transform;
      isPositionSet.current = true;
    }
  }, [x, y, parentHeight, parentWidth, transform, isLoaded]);

  return (
    <DraggableArea className={'disabled-element'} ref={ref} id={id}>
      <Background ref={videoRef} preload={"true"} autoplay playsinline muted loop>
        <source src={content} type={'video/mp4;codecs="avc1.42E01E, mp4a.40.2"'} />
      </Background>
    </DraggableArea>
  );
};

_DraggableVideo.propTypes = draggablePropTypes;

_DraggableVideo.defaultProps = {
  x: undefined,
  y: undefined,
  transform: '',
};

export const DraggableVideo = memo(_DraggableVideo);
