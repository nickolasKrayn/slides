import React, { useEffect, useRef, useState, forwardRef } from 'react';
import PropTypes from 'prop-types';

import { DraggableText } from '../DraggableText';
import { DraggableImage } from '../DraggableImage';
import { DraggableButton } from '../DraggableButton';
import { DraggableVideo } from '../DraggableVideo'
import { useTouch } from './useTouch';

import { Canvas } from './styles';

const elementComponents = {
  text: DraggableText,
  background: DraggableImage,
  video: DraggableVideo,
  button: DraggableButton
};

export const LayoutEditor = forwardRef(
  ({ elements, onElementTap }, elementsRef) => {
    const containerRef = useRef({});
    const { handleTouchStart, handleTouchMove, handleTouchEnd } = useTouch({
      elementsRef,
      containerRef,
      onElementTap,
    });
    const [containerBox, setContainerBox] = useState({
      x: 0,
      y: 0,
      height: 0,
      width: 0,
    });

    useEffect(() => {
      const container = containerRef.current;
      if (containerRef) {
        const { x, y } = container.getBoundingClientRect();
        setContainerBox({
          x,
          y,
          width: container.clientWidth,
          height: container.clientHeight,
        });
      }
    }, []);

    useEffect(() => {
      if (containerRef.current && Object.keys(elements).length > 0) {
        elementsRef.current = Array.from(containerRef.current.children);
      }
    }, [elements, elementsRef]);

    return (<>
      <Canvas
        ref={containerRef}
        onTouchStart={handleTouchStart}
        onTouchMove={handleTouchMove}
        onTouchCancel={handleTouchEnd}
        onTouchEnd={handleTouchEnd}
      >
        {Object.entries(elements).map(
          ([id, { x, y, type, content, background, transform, color }]) => {
            const Element = elementComponents[type];
            return (
              <Element
                key={id}
                content={content}
                id={id}
                background={background}
                color={color}
                x={x}
                y={y}
                transform={transform}
                parentHeight={containerBox.height}
                parentWidth={containerBox.width}
              />
            );
          }
        )}
      </Canvas>
    </>);
  }
);

LayoutEditor.propTypes = {
  elements: PropTypes.shape({
    [PropTypes.string]: PropTypes.shape({
      x: PropTypes.number,
      y: PropTypes.number,
      type: PropTypes.oneOf(['text', 'background', 'button', 'video']),
      content: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
      transform: PropTypes.string,
    }),
  }).isRequired,
  onElementTap: PropTypes.func,
};

LayoutEditor.defaultProps = {
  onElementTap: undefined,
};
