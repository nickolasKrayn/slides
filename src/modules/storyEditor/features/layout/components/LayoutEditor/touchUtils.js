export const getPinchProps = (touches) => {
  return {
    touches: Array.prototype.map.call(touches, (touch) => ({
      identifier: touch.identifier,
      pageX: touch.pageX,
      pageY: touch.pageY,
    })),
    ...(touches.length > 1
      ? {
          center: {
            x: (touches[0].pageX + touches[1].pageX) / 2,
            y: (touches[0].pageY + touches[1].pageY) / 2,
          },
          angle:
            (Math.atan2(
              touches[1].pageY - touches[0].pageY,
              touches[1].pageX - touches[0].pageX
            ) *
              180) /
            Math.PI,
          distance: Math.sqrt(
            Math.pow(Math.abs(touches[1].pageX - touches[0].pageX), 2) +
              Math.pow(Math.abs(touches[1].pageY - touches[0].pageY), 2)
          ),
        }
      : {
          center: {
            x: touches[0].pageX,
            y: touches[0].pageY,
          },
        }),
  };
};

export const getTransformProps = (style) => {
  const rotateMatch = /rotate\((?<rotate>[-?0-9\.]+)deg\)/g.exec(style);
  const scaleMatch = /scale\((?<scale>[0-9\.]+)\)/g.exec(style);
  return {
    rotate:
      rotateMatch && rotateMatch.groups.rotate
        ? Number(rotateMatch.groups.rotate)
        : 0,
    scale:
      scaleMatch && scaleMatch.groups.scale
        ? Number(scaleMatch.groups.scale)
        : 1,
  };
};

// find the number closest to n and divisible by m
export const findClosestNumber = (n, m) => {
  const q = Math.floor(n / m);
  const n1 = m * q;
  const n2 = n * m > 0 ? m * (q + 1) : m * (q - 1);
  return Math.abs(n - n1) < Math.abs(n - n2) ? n1 : n2;
};
