import styled from 'styled-components/macro';

import { Text as BaseText } from 'modules/storyEditor/components/elements/Text';

export const Text = styled(BaseText)`
  cursor: pointer;
  max-width: 100%;
`;

export const DraggableArea = styled.div`
  position: absolute;
  display: flex;
  z-index: 9;
  user-select: none;
  max-width: calc(100% - 128px);
  width: fit-content;
  padding: 0;
  left: 50%;
  top: 50%;
  -webkit-tap-highlight-color: transparent;
`;
