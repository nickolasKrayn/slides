import React, { useRef, useEffect, memo } from 'react';

import { draggablePropTypes } from '../../utils/draggablePropTypes';

import { Text, DraggableArea } from './styles';

const _DraggableText = ({
  id,
  x,
  y,
  content,
  transform,
  parentWidth,
  color,
  parentHeight,
}) => {
  const ref = useRef();
  const isPositionSet = useRef(false);

  useEffect(() => {
    const element = ref.current;
    if (
      ref.current &&
      !isPositionSet.current &&
      parentHeight > 0 &&
      parentWidth > 0
    ) {
      if (x || y) {
        element.style.top = y;
        element.style.left = x;
      } else {
        element.style.left = `${
          50 - (element.clientWidth / parentWidth / 2) * 100
        }%`;
        element.style.top = `${
          50 - (element.clientHeight / parentHeight / 2) * 100
        }%`;
      }
      element.style.transform = transform;
      if (color) {
        element.style.color = color;
      }
      isPositionSet.current = true;
    }
  }, [x, y, parentHeight, parentWidth, color, transform]);

  return (
    <DraggableArea ref={ref} id={id}>
      <Text style={{
        color: color
      }} dangerouslySetInnerHTML={{ __html: content }} />
    </DraggableArea>
  );
};

_DraggableText.propTypes = draggablePropTypes;

_DraggableText.defaultProps = {
  x: undefined,
  y: undefined,
  color: '#fff',
  transform: '',
};

export const DraggableText = memo(_DraggableText);
