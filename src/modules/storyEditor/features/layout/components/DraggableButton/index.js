import React, { useRef, useEffect, memo } from 'react';

import { draggablePropTypes } from '../../utils/draggablePropTypes';

import { Button, DraggableArea } from './styles';

const _DraggableButton = ({
  id,
  x,
  y,
  background,
  content,
  transform,
  parentWidth,
  parentHeight,
}) => {
  const ref = useRef();
  const isPositionSet = useRef(false);

  useEffect(() => {
    const element = ref.current;
    if (
      ref.current &&
      !isPositionSet.current &&
      parentHeight > 0 &&
      parentWidth > 0
    ) {
      if (x || y) {
        element.style.top = y;
        element.style.left = x;
      } else {
        element.style.left = `${
          50 - (element.clientWidth / parentWidth / 2) * 100
        }%`;
        element.style.top = `${
          50 - (element.clientHeight / parentHeight / 2) * 100
        }%`;
      }
      element.style.transform = transform;
      if (background) {
        element.style.background = background;
      } else {
        element.style.background = 'rgb(47, 128, 237)';
      }
      isPositionSet.current = true;
    }
  }, [x, y, parentHeight, parentWidth, transform, background]);

  return (
    <DraggableArea ref={ref} id={id}>
      <Button dangerouslySetInnerHTML={{ __html: content }} />
    </DraggableArea>
  );
};

_DraggableButton.propTypes = draggablePropTypes;

_DraggableButton.defaultProps = {
  x: undefined,
  y: undefined,
  transform: '',
  background: 'rgb(47, 128, 237)'
};

export const DraggableButton = memo(_DraggableButton);
