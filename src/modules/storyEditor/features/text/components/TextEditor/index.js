import React, { memo, useLayoutEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';

import { Overlay, TextInput, TextEditorWrapper, Container, Label } from './styles';
import { Wrapper, Button } from 'modules/storyEditor/components/Actions/styles';
import { ColorPicker } from './../../../../components/ColorPicker'
import Warning from './../../../../../../elements/Warning'
import icon_del from './../../../_img/delete-bin-line.svg'
import icon_check from './../../../_img/check-line.svg'

const moveCursorToEnd = (el) => {
  el.focus();
  if (el.innerText && document.createRange) {
    const selection = document.getSelection();
    const range = document.createRange();
    range.selectNodeContents(el);
    range.collapse(false);
    selection.removeAllRanges();
    selection.addRange(range);
    //range.detach();
  }
};

const _TextEditor = ({ id, initialText, initialColor, onSubmit, onRemove }) => {
  const inputRef = useRef();
  const [color, setColor] = useState(initialColor)

  useLayoutEffect(() => {
    if (inputRef.current) {
      const input = inputRef.current;
      moveCursorToEnd(input);
    }
  }, [initialText]);

  const handleSubmit = () => {
    onSubmit(inputRef.current.innerHTML, color);
  };

  const handleRemove = () => {
    onRemove(id)
  }

  return (
    <Overlay>
      <TextEditorWrapper>
        {id && <Warning onAction={handleRemove}>
          <Button style={{position: 'absolute', left: '15px'}}><img src={icon_del} /></Button>
        </Warning>}
        <Button onClick={handleSubmit} style={{position: 'absolute', right: '15px'}}><img src={icon_check} /></Button>
      </TextEditorWrapper>
      <Container style={{ width: 'calc(100% - 128px)'}}>
        <Label>Text:</Label>
        <TextInput
          ref={inputRef}
          contentEditable="true"
          tabIndex="-1"
          role="textbox"
          style={{
            color: color
          }}
          dangerouslySetInnerHTML={{ __html: initialText || 'text' }}
        ></TextInput>
        <ColorPicker
          label={`Color`}
          currentColor={color}
          onClick={color => setColor(color)}
        />
      </Container>
    </Overlay>
  );
};

_TextEditor.propTypes = {
  initialText: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
  onSubmit: PropTypes.func.isRequired,
};

_TextEditor.defaultProps = {
  initialText: '',
};

export const TextEditor = memo(_TextEditor);
