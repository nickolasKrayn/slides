import React, { memo, useLayoutEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import { Overlay, TextInput, Label, Container, ButtonEditorWrapper } from './styles';
import { Button } from 'modules/storyEditor/components/Actions/styles';
import { useState } from 'react';
import { ColorPicker } from './../../../../components/ColorPicker'
import Warning from './../../../../../../elements/Warning'
import icon_del from './../../../_img/delete-bin-line.svg'
import icon_check from './../../../_img/check-line.svg'

import { Text } from './../../../../elements/text'

const moveCursorToEnd = (el) => {
  el.focus();
  if (el.innerText && document.createRange) {
    const selection = document.getSelection();
    const range = document.createRange();
    range.selectNodeContents(el);
    range.collapse(false);
    selection.removeAllRanges();
    selection.addRange(range);
    //range.detach();
  }
};

const _ButtonEditor = ({ id, initialText, initialLink, initialBackground, onSubmit, onRemove }) => {
  const inputRef = useRef();
  const linkInputRef = useRef();
  const [color, setColor] = useState(initialBackground)

  useLayoutEffect(() => {
    if (inputRef.current) {
      const input = inputRef.current;
      moveCursorToEnd(input);
    }

  }, [initialText]);

  const handleSubmit = () => {
    onSubmit(inputRef.current.innerHTML, color, linkInputRef.current.innerHTML);
  };

  const handleRemove = () => {
    onRemove(id)
  }

  return (
    <Overlay>
      <ButtonEditorWrapper>
        {id && <Warning onAction={handleRemove}>
          <Button style={{position: 'absolute', left: '15px'}}><img src={icon_del} /></Button>
        </Warning>}
        <Button onClick={handleSubmit} style={{position: 'absolute', right: '15px'}}><img src={icon_check} /></Button>
      </ButtonEditorWrapper>
      <Container style={{ width: 'calc(100% - 128px)', maxWidth: '320px'}}>
          <Label>Text:</Label>
          <Text
            ref={inputRef}
            contentEditable="true"
            tabIndex="-1"
            role="textbox"
            dangerouslySetInnerHTML={{ __html: initialText || 'button' }}
          ></Text>
          <ColorPicker
            label={`Color:`}
            currentColor={color}
            onClick={(color) => {
              setColor(color)
            }}
          />
          <Label>Link:</Label>
          <TextInput
            ref={linkInputRef}
            contentEditable="true"
            tabIndex="-1"
            role="textbox"
            dangerouslySetInnerHTML={{ __html: initialLink }}
          ></TextInput>
      </Container>
    </Overlay>
  );
};

_ButtonEditor.propTypes = {
  initialText: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
  onSubmit: PropTypes.func.isRequired,
};

_ButtonEditor.defaultProps = {
  initialText: '',
};

export const ButtonEditor = memo(_ButtonEditor);
