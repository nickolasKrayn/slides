import styled from 'styled-components/macro';

import { Text } from 'modules/storyEditor/components/elements/Text';
import { Wrapper } from 'modules/storyEditor/components/Actions/styles';

export const Overlay = styled.div`
  display: flex;
  position: absolute;
  top: 0;
  left: 0;
  flex-direction: column;
  align-items: center;
  overflow: hidden;
  height: 100%;
  width: 100%;
  z-index: 16;
  background: #46bc82;
`;

export const Container = styled.div`
  display: block;
  padding: 0px 15px 0px 15px;
  width: calc(100% - 128px);
  max-width: 320px;
  position: absolute;
  transform: translateY(-50%);
  top: 50%;
`

export const Label = styled.div`
  display: block;
  width: 100%;
  font-size: 16px;
  color: #fff;
  opacity: .95;
  font-family: 'Nunito', sans-serif;
`

export const TextInput = styled(Text)`
  margin: 0px auto;
  padding: 5px 0;
  -webkit-user-select: text;
  user-select: text;
  text-align: left;
  border-bottom: 1px solid #3da773;
  &:focus {
    border-color: #338b60;
  }
`;

export const ButtonEditorWrapper = styled(Wrapper)`
  margin-top: 5px;
  width: 100%;
  right: 0;
`;

export const ColorContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-around;
`

export const Color = styled.div`
  width: 20%;
  display: flex;
`
