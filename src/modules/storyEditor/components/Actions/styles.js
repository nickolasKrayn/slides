import styled from 'styled-components/macro';

export const Wrapper = styled.div`
  display: flex;
  position: absolute;
  z-index: 10;
  right: 20px;
  top: 10px;

  > * + * {
    margin-left: 0px;
  }
`;

export const Button = styled.button`
  font-size: 32px;
  font-weight: normal;
  background-color: transparent;
  color: #fff;
  opacity: .95;
  border: 0;
  outline: 0;
  min-width: 40px;
  padding: 4px;
  text-align: center;
  font-size: 15px;
  cursor: pointer;
  & span {
    font-family: 'Nunito', sans-serif;
    text-shadow: 1px 1px #888;
  }
`;
