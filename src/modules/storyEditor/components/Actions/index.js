import React from 'react';
import PropTypes from 'prop-types';

import { Wrapper, Button } from './styles';

import { FaFileImage, FaPlayCircle, FaLink, FaTrashAlt, FaFont, FaWpforms } from 'react-icons/fa'
import Warning from './../../../../elements/Warning'
import icon_image from './img/remix/image-line.svg'
import icon_text from './img/remix/text.svg'
import icon_button from './img/remix/input-method-line.svg'
import icon_form from './img/remix/bill-line.svg'
import icon_video from './img/remix/video-line.svg'
import icon_del from './img/remix/delete-bin-line.svg'

const picSize = '30px'

const actions = [
  { icon: <span><img src={icon_image} /> <span style={{display: 'block'}}>Image</span></span>, name: 'image' },
  { icon: <span><img src={icon_text} /> <span style={{display: 'block'}}>Text</span></span>, name: 'text' },
  { icon: <span><img src={icon_button} /> <span style={{display: 'block'}}>Button</span></span>, name: 'button' },
  { icon: <span><img src={icon_form} /> <span style={{display: 'block'}}>Forms</span></span>, name: 'form' },
  { icon: <span><img src={icon_video} /> <span style={{display: 'block'}}>Video</span></span>, name: 'video' },
  /*{ icon: <span><img src={icon_del} /> <span style={{display: 'block'}}>Delete</span></span>, name: 'delete' }*/
];

export const Actions = ({ onActionClick }) => (
  <Wrapper style={{
    flexDirection: 'column',
    top: '50%',
    right: '0',
    transform: 'translateY(-55%)'
  }}>
    {actions.map(({ icon, name }) => (
      name === 'delete' ? <Warning onAction={() => {
        onActionClick(name)
      }}>
        <Button style={{
          marginTop: '20px',
          width: '70px',
          textAlign: 'center',
          fontSize: '13px',
        }}>{icon}</Button>
      </Warning> : <Button style={{
        marginTop: '20px',
        width: '70px',
        textAlign: 'center',
        fontSize: '13px'
      }} onClick={() => onActionClick(name)}>{icon}</Button>
    ))}
  </Wrapper>
);

Actions.propTypes = {
  onActionClick: PropTypes.func.isRequired,
};
