import React, { useState, useRef, useCallback } from 'react';

import { createUUID } from 'utils/createUUID';
import { Actions } from '../Actions';
import { Layout } from '../Layout';
import { TextEditor } from '../../features/text/components/TextEditor';
import { ButtonEditor } from '../../features/button/components/ButtonEditor'
import { FormEditor } from '../../features/form/components/FormEditor'
import { LayoutEditor } from '../../features/layout/components/LayoutEditor';
import { ImageEditor } from '../../features/image/components/ImageEditor'
import { VideoEditor } from '../../features/video/components/VideoEditor'

import { ImageUploader } from '../ImageUploader';
import { VideoUploader } from '../VideoUploader'

import { FaArrowLeft, FaArrowRight } from 'react-icons/fa'
import { useEffect } from 'react';
import userEvent from '@testing-library/user-event';

const PrevNextControllers = () => {
  return (<div style={{
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    color: '#fff'
  }}>
  <div style={{ padding: '20px' }}>
    <FaArrowLeft />
  </div>
  <div style={{ padding: '20px' }}>
    <FaArrowRight />
  </div>
  </div>)
}

export const StoryEditor = ({ onRemove, onChangeElements, items }) => {
  const [currentEditor, setCurrentEditor] = useState('main');
  const [elements, setElements] = useState(items.elements);
  const [currentElementId, setCurrentElementId] = useState(null);
  let elementsRef = useRef(items.elementsRef);
  const imageUploaderRef = useRef();
  const videoUploaderRef = useRef();

  useEffect(() => { // TODO: Хуйню какую-то накодил, это из-за того что торопят. Всяко реще, чем setElements'ы переделывать, которые я нахуярил копипастом. Перекодить нормально.
    setElements(items.elements)
    elementsRef = items.elementsRef
  }, [items])

  useEffect(() => {
    onChangeElements({
      elements: elements,
      elementsRef: elementsRef
    })
  }, [elements, elementsRef])

  const copyElement = (elementInfo, elementNode) => {
    return {
      type: elementInfo.type,
      content: elementInfo.content,
      color: elementInfo.color,
      x: elementNode.style.left,
      y: elementNode.style.top,
      transform: elementNode.style.transform,
      link: elementInfo.link,
      background: elementInfo.background
    }
}

  const removeAllBackground = () => {
    if (elementsRef && elementsRef.current && elementsRef.current.reduce) {
      setElements(
        elementsRef.current.reduce((acc, elementNode) => {
        const elementInfo = elements[elementNode.id];
        if (elementInfo.type !== 'background' && elementInfo.type !== 'video') {
          return {
            ...acc,
            [elementNode.id]: copyElement(elementInfo, elementNode)
          }
        }
          return acc
        }, {})
      )
    }
  }

  const addElement = useCallback((info) => {
    setElements((prevElements) => ({
      ...prevElements,
      [createUUID()]: info,
    }));
  }, []);

  const addImage = async (image) => {
    removeAllBackground()
    const blobUrl = URL.createObjectURL(image);
    addElement({
      type: 'background',
      content: blobUrl,
      transform: '',
    });
  };

  const onRemoveElement = (elementId) => {
    setElements( // делает сейв состояния элементов
      elementsRef.current.reduce((acc, elementNode) => {
        const elementInfo = elements[elementNode.id];
        if (elementInfo) {
          return {
            ...acc,
            [elementNode.id]: copyElement(elementInfo, elementNode),
          };
        }
      }, {})
    );
    setElements(prevElements => {
      const copy = prevElements
      delete copy[elementId]
      return copy
    })

    setCurrentElementId(null)
    setCurrentEditor('main')
  }

  const onActionClick = async (editorName) => {
    if (editorName === 'image') {
      return imageUploaderRef.current.open()
    }
    if (editorName === 'video') {
      return videoUploaderRef.current.open()
    }
    if (editorName === 'delete') {
      onRemove()
      return
    }
    //if (editorName === 'button') {
    //  addButton()
    //  return
    //}
    setCurrentEditor(editorName);
  };

  const onTextSubmit = (text, color) => {
    setElements(
      elementsRef.current.reduce((acc, elementNode) => {
        const elementInfo = elements[elementNode.id];
        if (elementInfo) {
          return {
            ...acc,
            [elementNode.id]: copyElement(elementInfo, elementNode),
          };
        }
      }, {})
    );
    if (currentElementId) {
      setElements((prevElements) => ({
        ...prevElements,
        [currentElementId]: {
          ...prevElements[currentElementId],
          content: text,
          color: color
        },
      }));
      setCurrentElementId(null);
    } else {
      addElement({
        type: 'text',
        content: text,
        transform: '',
        color: color
      });
    }
    setCurrentEditor('main');
  };

  const onImageSubmit = (content) => {
    if (content) {
      setElements(
        elementsRef.current.reduce((acc, elementNode) => {
          const elementInfo = elements[elementNode.id];
          if (elementInfo) {
            return {
              ...acc,
              [elementNode.id]: copyElement(elementInfo, elementNode),
            };
          }
        }, {})
      );

      setElements((prevElements) => ({
        ...prevElements,
        [currentElementId]: {
          ...prevElements[currentElementId],
          content: content
        },
      }));
    }
    setCurrentElementId(null);
    setCurrentEditor('main')
  }

  const onVideoSubmit = (content) => {
    if (content) {
      setElements(
        elementsRef.current.reduce((acc, elementNode) => {
          const elementInfo = elements[elementNode.id];
          if (elementInfo) {
            return {
              ...acc,
              [elementNode.id]: copyElement(elementInfo, elementNode),
            };
          }
        }, {})
      );

      setElements((prevElements) => ({
        ...prevElements,
        [currentElementId]: {
          ...prevElements[currentElementId],
          content: content
        },
      }));
    }
    setCurrentElementId(null);
    setCurrentEditor('main')
  }

  const onButtonSubmit = (text, background, link) => {
    if (!text || text === '') {
      setCurrentEditor('main');
      return
    }

    setElements(
      elementsRef.current.reduce((acc, elementNode) => {
        const elementInfo = elements[elementNode.id];
        if (elementInfo) {
          return {
            ...acc,
            [elementNode.id]: copyElement(elementInfo, elementNode),
          };
        }
      }, {})
    );
    if (currentElementId) {
      setElements((prevElements) => ({
        ...prevElements,
        [currentElementId]: {
          ...prevElements[currentElementId],
          link: link,
          content: text,
          background: background
        },
      }));
      setCurrentElementId(null);
    } else {
      addElement({
        type: 'button',
        content: text,
        link: link,
        transform: '',
        background: background
      });
    }
    setCurrentEditor('main');
  };

  const onElementTap = (id) => {
    setCurrentElementId(id);
    
    if (elements[id] && elements[id].type === 'background') {
      console.log(elements[id])
      setCurrentEditor('image');
    }
    if (elements[id] && elements[id].type === 'text') {
      setCurrentEditor('text');
    }
    if (elements[id] && elements[id].type === 'button') {
      setCurrentEditor('button');
    }
    if (elements[id] && elements[id].type === 'video') {
      setCurrentEditor('video');
    }
  };

  const addVideo = (video) => {
    removeAllBackground()
    const blobUrl = URL.createObjectURL(video);
    addElement({
      type: 'video',
      content: blobUrl,
      transform: '',
    });
  }

  return (
    <Layout>
      {currentEditor === 'main' && (
        <>
          {/*<PrevNextControllers />*/}
          <Actions onActionClick={onActionClick} />
          <LayoutEditor
            elements={elements}
            ref={elementsRef}
            onElementTap={onElementTap}
          />
        </>
      )}
      {currentEditor === 'text' && (
        <TextEditor
          id={currentElementId}
          onSubmit={onTextSubmit}
          onRemove={onRemoveElement}
          {...(elements[currentElementId] && {
            initialText: elements[currentElementId].content,
            initialColor: elements[currentElementId].color,
          })}
        />
        )}
      {currentEditor === 'image' && (
        <ImageEditor
          id={currentElementId}
          onSubmit={onImageSubmit}
          onRemove={onRemoveElement}
          {...(elements[currentElementId] && {
            initialContent: elements[currentElementId].content
          })}
        />
      )}
      {currentEditor === 'button' && (
        <ButtonEditor
          id={currentElementId}
          onSubmit={onButtonSubmit}
          onRemove={onRemoveElement}
          {...(elements[currentElementId] && {
            initialText: elements[currentElementId].content,
            initialLink: elements[currentElementId].link,
            initialBackground: elements[currentElementId].background,
          })}
        />
        )}
      {currentEditor === 'form' && (
        <FormEditor
          id={currentElementId}
          onSubmit={onButtonSubmit}
          onRemove={onRemoveElement}
          {...(elements[currentElementId] && {
            initialText: elements[currentElementId].content,
            initialLink: elements[currentElementId].link,
            initialBackground: elements[currentElementId].background,
          })}
        />
        )}
      {currentEditor === 'video' && (<VideoEditor
        id={currentElementId}
        onSubmit={onVideoSubmit}
        onRemove={onRemoveElement}
        {...(elements[currentElementId] && {
          initialContent: elements[currentElementId].content
        })}
      />)}
      <ImageUploader ref={imageUploaderRef} onImageSelect={addImage} />
      <VideoUploader ref={videoUploaderRef} onVideoSelect={addVideo} />
    </Layout>
  );
};
