import React, { useRef, useImperativeHandle, forwardRef } from 'react';

export const VideoUploader = forwardRef(({ onVideoSelect }, ref) => {
  const inputRef = useRef();

  const onInputChange = () => {
    const files = Array.from(inputRef.current.files);
    onVideoSelect && onVideoSelect(files[0]);
    if (inputRef && inputRef.current && inputRef.current.value) {
      inputRef.current.value = ''
    }
  };

  useImperativeHandle(
    ref,
    () => ({
      open: () => inputRef.current.click(),
    }),
    []
  );

  return (
    <>
      <input
        style={{ display: 'none' }}
        onChange={onInputChange}
        accept="video/*"
        type="file"
        ref={inputRef}
      />
    </>
  );
});
