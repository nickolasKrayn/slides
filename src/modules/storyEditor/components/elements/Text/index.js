import styled from 'styled-components/macro';

export const Text = styled.span`
  display: block;
  text-align: center;
  word-wrap: break-word;
  color: white;
  text-shadow: rgba(150, 150, 150, 0.3) 0px 1px 2px;
  font-size: 20px;
  font-weight: 400;
  line-height: 32px;
  outline: 0;
  padding: 10px;
  margin: 0 auto;
`;
