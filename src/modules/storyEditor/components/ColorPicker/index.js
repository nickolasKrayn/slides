import React from 'react'
import { Container, ColorElement, Label } from './styles'

const _ColorPicker = ({ currentColor, colors, onClick, label }) => {
    return <>
    {label && <Label>{label}</Label>}
    <Container>
        {colors.map((color) => {
            const additionalStyles = {}

            if (color === currentColor) {
                additionalStyles.boxShadow = '0 0 0 2px #2E7D57, 0 0 0 4px #3CA372'
            }

            return <ColorElement
                key={color}
                style={{ background: color, ...additionalStyles }}
                onClick={() => onClick(color)}
        />})}
    </Container></>
}

_ColorPicker.defaultProps = {
    colors: ['#d8d8d8', '#b34d4b', '#5897e6', '#a62bb7', '#bdcb64', '#4ee7b8'],
};

export const ColorPicker = _ColorPicker