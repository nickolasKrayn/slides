import styled from 'styled-components/macro';

export const Container = styled.div`
    display: flex;
    width: 100%;
    justify-content: space-between;
    margin: 10px auto 20px;

`

export const ColorElement = styled.div`
    display: flex;
    width: 36px;
    height: 36px;
    border-radius: 20px;
    margin-left: 9px;
    &:first-child {
        margin-left: 0;
    }

`

export const Label = styled.div`
    display: block;
    font-size: 16px;
    width: 100%;
    color: #fff;
    opacity: .95;
    font-family: 'Nunito', sans-serif;
`