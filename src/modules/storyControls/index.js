import React from 'react'
import Warning from './../../elements/Warning'
import { Container, ButtonContainer, Button, MenuButton } from './style'
import icon_add from './img/add-box-line.svg'
import icon_delete from './img/delete-bin-line.svg'
import icon_publication from './img/upload-2-line.svg'
import icon_burger from './img/menu-line.svg'

export const StoryControls = ({ onAdd, onRemove }) => {
    return <Container>
        <MenuButton><img src={icon_burger} /></MenuButton>
        <ButtonContainer>
            <Warning onAction={onRemove}>
                <Button><img src={icon_delete} /></Button>
            </Warning>
            <Button onClick={onAdd}><img src={icon_add} /></Button>
            <Button><img src={icon_publication} /></Button>
        </ButtonContainer>
    </Container>
}