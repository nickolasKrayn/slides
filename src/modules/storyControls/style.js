import styled from 'styled-components/macro'

export const Container = styled.div`
    // bottom: 0;
    // left: 0;
    // position: absolute;
    // z-index: 10;
    width: 100%;
    // height: 68px;
    height: 0;
`;

export const ButtonContainer = styled.div`
    max-width: 200px;
    margin: auto;
    position: absolute;
    bottom: 0;
    right: 0;
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    color: #fff;
    font-family: 'Nunito', sans-serif;
    max-width: 300px;
    font-size: 13px;
    opacity: .95;
    z-index: 10;
`;

export const Button = styled.div`
    cursor: pointer;
    text-align: center;
    border: 1px solid #000;
    border-radius: 20px;
    padding: 20px;
    margin: 0;
    border: none;
    display: flex;
    flex-flow: column;
    & > img {
        width: 28px;
        height: 28px;
    }
`;

export const MenuButton = styled.div`
    position: absolute;
    z-index: 10;
    left: 0;
    padding: 20px;
    bottom: 0;
    & > img {
        width: 28px;
        height: 28px;
    }
`;