import React, { useState } from 'react';

import { GlobalStyle } from './globalStyles';
import { Slider, FakeSlider } from 'modules/slider'

import { createUUID } from 'utils/createUUID'

const App = () => { // TODO: Сделать нормально
  const initSlideId = createUUID()

  const [activeSlide, setActiveSlide] = useState(0)
  const [ slides, setSlides ] = useState([{
    id: initSlideId,
    contentType: 'storyEditor'
  }])

  const removeSlide = (slideId) => {
    setSlides(slides.filter(slide => slide.id !== slideId))
    if (activeSlide === slides.length - 1) {
      setActiveSlide(activeSlide - 1)
    }
  }

  const addSlide = (slideId) => {
    setSlides([...slides, {
      id: slideId,
      contentType: 'storyEditor'
    }])
  }

  return <>
    <GlobalStyle />
    <FakeSlider
      activeSlide={activeSlide}
      setActiveSlide={setActiveSlide}
      removeSlide={removeSlide}
      addSlide={addSlide}
      slides={slides} />
  </>
};

export default App;
