export const vibrate = (ms) =>
  window.navigator.vibrate && window.navigator.vibrate(ms);
