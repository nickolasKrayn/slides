import React, { Component, Fragment } from 'react'
import Modal from 'react-modal'
import { FaTimes } from 'react-icons/fa'
import './index.css'
import closeImg from './img/close.svg'


Modal.setAppElement('#root')
export default class extends Component {
    constructor(props) {
        super(props)

        /*if (props.isOpenModal === undefined) {
            throw Error('свойство isOpenModal не проинициализированно');
        }*/

        if (props.deactivateModal === undefined) {
            throw Error('свойство deactivateModal не проинициализированно');
        }

        this.getApplicationNode = this.getApplicationNode.bind(this);
    }

    getApplicationNode() {
        return document.getElementById('root');
    }

    createModal() {
        const modal = <Modal
            isOpen={this.props.isOpenModal}
            onRequestClose={this.props.deactivateModal}
            className={this.props.modalContentClass ? this.props.modalContentClass : "modal-content drag-constructor-modal"}
            overlayClassName="modal-overlay"
        >
            <div className="modal-content-body" style={this.props.style ? { ...this.props.style } : {}}>
                <div className='modal-pop-up_close' style={!this.props.isClosable ? { color: '#000' } : { display: 'none' }} onClick={this.props.deactivateModal}><img src={closeImg} alt="close"/></div>
                {this.props.children}
            </div>
            <div className='modal-pop-up__filler' onClick={this.props.deactivateModal} />
        </Modal>

        return (
            <Fragment>
                {modal}
            </Fragment>
        );
    }

    render() {
        return this.createModal();
    }
}
