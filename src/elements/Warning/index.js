import React, { Component } from 'react'
import Modal from './../Modal'

class RemoveAttentionModal extends Component {
    constructor (props) {
        super (props)

        this.state = {
            isOpenModal: false
        }

        this.handleCloseModal = this.handleCloseModal.bind(this)
        this.handleOpenModal = this.handleOpenModal.bind(this)
        this.handleRemove = this.handleRemove.bind(this)
    }

    handleOpenModal () {
        this.setState({
            isOpenModal: true
        })
    }

    handleCloseModal () {
        this.setState({
            isOpenModal: false
        })
    }

    handleRemove () {
        this.props.onAction()
        this.handleCloseModal()
    }

    render () {
        const { info, children } = this.props
        const { isOpenModal } = this.state

        return (<React.Fragment>
            <span onClick={this.handleOpenModal}>
                {children}
            </span>
            <Modal isOpenModal={isOpenModal} deactivateModal={this.handleCloseModal}>
                <h2 style={{ fontSize: '23px' }}>Warning!</h2>
                <div style={{
                    marginBottom: '30px'
                }}>
                    Are you sure?
                </div>
                <div style={{ display: 'flex', justifyContent: 'stretch'}}>
                    <button onClick={this.handleRemove} style={{ borderRadius: '2px' }} className='btn btn-danger'>
                        remove
                    </button>
                    <button className='btn btn-purple' style={{ borderRadius: '2px' }} onClick={this.handleCloseModal}>
                        cancel
                    </button>
                </div>
            </Modal>
        </React.Fragment>)
    }
}

export default RemoveAttentionModal
