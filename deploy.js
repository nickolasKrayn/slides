require('dotenv').config()

const AWS = require('aws-sdk')
const mime = require('mime-types')
const fs = require('fs')
const { join } = require('path')

const endpoint = new AWS.Endpoint('storage.yandexcloud.net')
const s3 = new AWS.S3({
  endpoint: endpoint,
  accessKeyId: process.env.YANDEX_STORAGE_KEY_ID,
  secretAccessKey: process.env.YANDEX_STORAGE_ACCESS_KEY,
  region: 'us-east-1',
  httpOptions: {
    timeout: 10000,
    connectTimeout: 10000
  }
})

const root = 'build'

const isDirectory = path => fs.statSync(path).isDirectory()
const getDirectories = path => fs.readdirSync(path).map(name => join(path, name)).filter(isDirectory)

const isFile = path => fs.statSync(path).isFile()
const getFiles = path => fs.readdirSync(path).map(name => join(path, name)).filter(isFile)

const getFilesRecursively = (path) => {
  const dirs = getDirectories(path)
  const files = dirs
    .map(dir => getFilesRecursively(dir))
    .reduce((a, b) => a.concat(b), [])
  return files.concat(getFiles(path))
}

const transformToRelativePath = (file) => {
  let isRelative = false
  const relativePath = file.split('\\').map(fragment => {
    if (isRelative) {
      return fragment
    }
    if (fragment === root) {
      isRelative = true
    }
  }).filter(fragment => fragment !== undefined)

  return {
    absolute: file,
    relative: relativePath.join('/')
  }
}

const getObjects = async (bucket) => {
  const raw = await s3.listObjects({ Bucket: bucket }).promise()
  return raw.Contents.map((c) => {
    return {
      Key: c.Key
    }
  })
}

const main = async () => {
  const buildPath = join(__dirname, root)
  //const bucket = 'storyland-frontend'
  const bucket = 'drag-stage'

  //fs.copyFileSync(__dirname + '/shit.html', buildPath + '/policy.html')

  const files = getFilesRecursively(buildPath)
    .map(transformToRelativePath)
    .map(file => {
      file.mime = mime.lookup(file.relative)
      file.bin = fs.readFileSync(file.absolute)
      return file
    })
/*
  const cache = await getObjects(bucket)
  const keys = cache.map(c => {
    let loc = files.find(f => { // eslint-disable-line
      if (f.relative === c.Key) {
        return true
      }
    })

    if (!loc) {
      return c
    }
  }).filter(f => f !== undefined)

  const dropParams = {
    Bucket: bucket,
    Delete: {
      Objects: keys,
      Quiet: false
    }
  }
*/
  const promises = files.map(file => {
    const params = {
      Bucket: bucket,
      Key: file.relative,
      Body: file.bin,
      ContentType: file.mime
    }

    return s3.upload(params).promise()
  })

  await Promise.all([
    ...promises,
    //s3.deleteObjects(dropParams).promise()
  ])
}

main()
  .then(() => console.log('Deploy Complete'))
  .catch((e) => console.log('Deploy Error: ' + e))
